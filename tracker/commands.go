package tracker

import (
	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

// CStats cccccounts the amount of known emotes
var CStats = &drc.Command{
	Name:         "stats",
	Manual:       []string{"lists the known emote stats"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		BotAdmin: trit.True,
	},
	Config: drc.CfgCommand{
		Listable: true,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 0,
	},
	Exec: fStats,
}

func fStats(ctx *drc.Context) error {
	ct, err := getEmoteCount()
	if err != nil {
		return err
	}
	uct, err := getEmoteUniqueCount()
	if err != nil {
		return err
	}
	mct, err := getMesEmoteCountGlobal()
	if err != nil {
		return err
	}
	rct, err := getReactEmoteCountGlobal()
	if err != nil {
		return err
	}
	gmct, err := getMesEmoteCountByGuild(ctx.Mes.GuildID)
	if err != nil {
		return err
	}
	grct, err := getReactEmoteCountByGuild(ctx.Mes.GuildID)
	if err != nil {
		return err
	}
	return ctx.Replyf("There are %v known emotes, and %v known unique emotes\nGlobally there have been %v reactions, and %v emotes counted in messages\nWithin this server there have been %v reactions, and %v emotes counted in messages",
		ct, uct, rct, mct, grct, gmct)
	//KnownEmotes.ItemCount(), ReactionEmotes.ItemCount(), MessageEmotes.ItemCount())
}

// CTop is a command to output the most used emotes, however at this point it is only a debug command
var CTop = &drc.Command{
	Name:         "top",
	Manual:       []string{"lists the top emotes used in messages"},
	CommandPerms: discordgo.PermissionSendMessages,
	Permissions: drc.Permissions{
		BotAdmin: trit.True,
	},
	Config: drc.CfgCommand{
		Listable: true,
		ReactOn: drc.ActOn{
			Success: trit.True,
		},
		MinimumArgs: 0,
	},
	Exec: fTop,
}

func fTop(ctx *drc.Context) error {
	top, err := getTopMesEmoteCountByHash(ctx.Mes.GuildID, 10)
	if err != nil {
		return err
	}
	Emotes := make([][]Emote, 0)
	for _, x := range top {
		em, _, err := getEmoteByShortHash(x.Hash)
		if err != nil {
			return err
		}
		Emotes = append(Emotes, em)
	}
	return ctx.Replyf("```\n%v\n```", top)
}
