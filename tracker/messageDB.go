package tracker

import (
	"time"

	"github.com/gocraft/dbr/v2"
)

// Message contains a database description of a single emote on a message,
// multiple of these can exist for a single message as one of these will exist per emote used in a message
type Message struct {
	Message   string    `db:"message_id"`
	Guild     string    `db:"guild_id"`
	Channel   string    `db:"channel_id"`
	User      string    `db:"user_id"`
	EmoteID   string    `db:"emote_id"`
	EmoteHash string    `db:"emote_hash"`
	Timestamp time.Time `db:"timestamp"`
}

// addMessage inserts a message emote into the database
func addMessage(message Message) error {
	_, err := ss.InsertInto("messages").
		Columns("message_id", "guild_id", "channel_id", "user_id", "emote_id", "emote_hash", "timestamp").
		Record(message).Exec()
	return err
}

// addMessages adds a slice of message emotes into the database
func addMessages(messages []Message) error {
	for _, m := range messages {
		err := addMessage(m)
		if err != nil {
			return err
		}
	}
	return nil
}

// getMessagesByID fetches a slice of message emotes by the message id
func getMessagesByID(id string) (message []Message, found bool, err error) {
	i, err := ss.Select("*").
		From("messages").
		Where(dbr.Eq("message_id", id)).
		Load(&message)

	if i < 1 {
		return
	}
	return message, true, err
}

// deleteMessagesByID deletes all message emotes assosiated with a message
func deleteMessagesByID(id string) (err error) {
	_, err = ss.DeleteFrom("messages").Where(dbr.Eq("message_id", id)).Exec()
	return err
}

// getMesEmoteCountByGuild returns how many emotes have been used in messages within a server
func getMesEmoteCountByGuild(guild string) (count int, err error) {
	i, err := ss.Select("COUNT(emote_id)").
		From("messages").
		Where(dbr.Eq("guild_id", guild)).
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}

// getMesEmoteCountGlobal returns how many emotes have been used in messages globally
func getMesEmoteCountGlobal() (count int, err error) {
	i, err := ss.Select("COUNT(emote_id)").
		From("messages").
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}

// getMesEmoteCountByID gets the usage of an emote within a guild
func getMesEmoteCountByID(id string, guild string) (count int, err error) {
	i, err := ss.Select("COUNT(emote_id)").
		From("messages").
		Where(dbr.Eq("emote_id", id)).
		Where(dbr.Eq("guild_id", guild)).
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}

// getMesEmoteCountByHash gets the usage of an emote hash within a guild (it gets the use of the hash, which includes duplicates of the same emote)
func getMesEmoteCountByHash(hash string, guild string) (count int, err error) {
	i, err := ss.Select("COUNT(emote_hash)").
		From("messages").
		Where(dbr.Eq("emote_hash", hash)).
		Where(dbr.Eq("guild_id", guild)).
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}

// Top is a type used for a sort of top x of emotes
type Top struct {
	Count uint64
	Hash  string `db:"emote_hash"`
	ID    string `db:"emote_id"`
}

// getTopMesEmoteCountByHash gets the most used emotes in a guild counting by hash up to the amount defined by limit
func getTopMesEmoteCountByHash(guild string, limit uint64) (count []Top, err error) {
	i, err := ss.Select("COUNT(message_id), emote_hash").
		From("messages").
		Where(dbr.Eq("guild_id", guild)).
		GroupBy("emote_hash").
		OrderDesc("COUNT(message_id)").
		Limit(limit).
		Load(&count)

	if i < 1 {
		return
	}
	return count, err
}
