package tracker

import (
	"bytes"
	"fmt"
	"log"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/emotetracker/module"
	"github.com/burntsushi/toml"
	"github.com/gocraft/dbr/v2"

	// Import postgresql for database stuff
	_ "github.com/lib/pq"
)

var dbc *dbr.Connection

var ss *dbr.Session

// Config is the config that contains global config stuff, i mean it is named config so what else would it be
var Config = &localConfig{}

type localConfig struct {
	Name, User, Pass, Host string
	SaveTo                 string
	Debug                  bool
}

// Module contains the module, i mean what else would it contain
var Module = &module.Module{
	Name: "tracker",
	DgoHandlers: []interface{}{
		onMessage,
		messageDelete,
		messageDeleteBulk,
		messageUpdate,
		reactionAdd,
		reactionRemove,
		reactionBulkRemove,
	},

	Commands: []*drc.Command{
		CStats,
		CTop,
	},

	DebugCommands: []*drc.Command{
		DScanchannel,
		DScanserver,
	},

	Config: Config,

	InitFunc: func(mod *module.Module) error {
		if !mod.ConfigFound {
			b := bytes.NewBuffer([]byte{})
			e := toml.NewEncoder(b)
			err := e.Encode(mod.Config)
			if err != nil {
				return err
			}
			fmt.Println(b.String())
			return fmt.Errorf("Config not found, database config data mandatory")
		}
		return nil
	},

	OpenFunc:  OpenFunc,
	CloseFunc: CloseFunc,
}

// OpenFunc contains some the initialisation functions for this module per the terms of the pre-alpha attempt at modularisation this is built off of
func OpenFunc(mod *module.Module) error {
	dsn := fmt.Sprintf("dbname=%v user=%v password='%v' host='%v' sslmode=disable",
		Config.Name, Config.User, Config.Pass, Config.Host)
	conn, err := dbr.Open("postgres", dsn, nil)
	if err != nil {
		return err
	}
	if err := conn.Ping(); err != nil {
		return err
	}

	dbc = conn

	ss = dbc.NewSession(nil)
	return nil
}

// CloseFunc contains the closing functions for this module per the terms of the pre-alpha attempt at modularisation this is built off of
func CloseFunc(mod *module.Module) {
	log.Println(ss.Close())
	log.Println(dbc.Close())
}
