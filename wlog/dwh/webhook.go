package dwh

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

// Webhook is a webhook client
type Webhook struct {
	Client *http.Client
	URL    string
}

// NewWebhook returns a new webhook with a set URL
func NewWebhook(url string) *Webhook {
	return &Webhook{
		Client: &http.Client{},
		URL:    url,
	}
}

// SendMessage sends a webhookMessage to the url within the client
func (w Webhook) SendMessage(contents WebhookMessage) error {
	return w.pushWebhookToURL(w.URL, contents)
}

// pushWebhookToURL pushes a webhook message to a custom webhook url seperate to the one in the host
func (w Webhook) pushWebhookToURL(url string, contents WebhookMessage) error {
	data, err := toJSON(contents)
	if err != nil {
		return err
	}

	buf := strings.NewReader(data)

	s, err := w.httpPost(url, "application/json", buf)
	if err != nil {
		return err
	}
	if s != "" {
		return fmt.Errorf(s)
	}
	return nil
}

// httpPost posts some data to a url
func (w Webhook) httpPost(url, mimetype string, data io.Reader) (string, error) {
	// send http post request with the json data to the discord webhook url
	resp, err := w.Client.Post(url, mimetype, data)
	if err != nil {
		return "", fmt.Errorf("dwh.httpPost: %w", err)
	}
	defer resp.Body.Close()

	// read response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("dwh.httpPost: body: %v | err: %w", string(body), err)
	}
	return string(body), nil
}

// toJSON is a simple function that just wraps converts a struct to a string containing json
// basically just wraps the standard json lib
func toJSON(data interface{}) (string, error) {
	d, err := json.Marshal(data)
	if err != nil {
		return "", fmt.Errorf("dwh.toJSON: %w", err)
	}
	return string(d), nil
}
